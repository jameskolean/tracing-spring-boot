package com.example.webapp;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class RootController {
	@Autowired
	RestTemplate restTemplate;

	@Value( "${service.base.url}" )
	private String serviceBaseUrl;

	@GetMapping("/")
	@NewSpan("RootController#helloWorld")
	@ResponseBody
	public String helloWorld() {
		log.info("Handling home");
		return restTemplate.getForEntity(serviceBaseUrl+"/", String.class).getBody();
	}
}
