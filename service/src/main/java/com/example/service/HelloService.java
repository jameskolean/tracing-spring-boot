package com.example.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import brave.ScopedSpan;
import brave.Tracer;

@Service
public class HelloService {
	@Autowired
	private Tracer tracer;

	SimpleDateFormat timeFormater = new SimpleDateFormat("hh:mm:ss");

	@NewSpan("HelloService#createMessage with explicit span")
	public String createMessage() {
		ScopedSpan span = tracer.startScopedSpan("Add date to hello world");
		String message = "Hello World at " + timeFormater.format(new Date());
		span.finish();
		return message;
	}
}
