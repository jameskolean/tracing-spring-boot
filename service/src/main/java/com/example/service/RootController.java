package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class RootController {
	@Autowired
	HelloService helloService;

	@GetMapping("/")
	@NewSpan("RootController#helloWorld")
	@ResponseBody
	public String helloWorld() {
		log.info("Handling home");
		return helloService.createMessage();
	}
}
